package io.gitlab.cylearningit.designpatterns.bridge.naive;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class BridgeNaiveClient {
    public static void main(String[] args) {
        Shape s1 = new V1Rectangle(1,2,3,4);
        Shape s2 = new V2Circle(1, 2, 3);

        s1.draw();
        s2.draw();
    }
}
