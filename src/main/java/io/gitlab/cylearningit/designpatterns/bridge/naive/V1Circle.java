package io.gitlab.cylearningit.designpatterns.bridge.naive;

import io.gitlab.cylearningit.designpatterns.bridge.provided.DP1;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class V1Circle extends Circle {
    public V1Circle(double x, double y, double r) {
        super(x, y, r);
    }

    @Override
    protected void drawCircle(double x, double y, double r) {
        DP1.draw_a_circle(x, y, r);
    }
}
