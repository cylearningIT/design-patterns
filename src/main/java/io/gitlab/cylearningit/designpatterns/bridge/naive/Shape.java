package io.gitlab.cylearningit.designpatterns.bridge.naive;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public abstract class Shape {
    public abstract void draw();
}
