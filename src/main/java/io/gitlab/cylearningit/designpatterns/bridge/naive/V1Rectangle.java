package io.gitlab.cylearningit.designpatterns.bridge.naive;

import io.gitlab.cylearningit.designpatterns.bridge.provided.DP1;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class V1Rectangle extends Rectangle {
    public V1Rectangle(double x1, double y1, double x2, double y2) {
        super(x1, y1, x2, y2);
    }

    @Override
    protected void drawLine(double x1, double y1, double x2, double y2) {
        DP1.draw_a_line(x1, y1, x2, y2);
    }
}
