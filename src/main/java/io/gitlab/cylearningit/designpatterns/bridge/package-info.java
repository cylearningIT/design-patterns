/**
 * Classes for exploring the Bridge Design Pattern <br/> <br/>
 * <h2>Summary</h2>
 * The Bridge design pattern is useful when one set of variations is using another set of variations.
 * In such a case it allows the design and implementation are more robust and more flexible to handle changes in the future,
 * by allowing both sets of variation to be modifiable on their own.
 *
 * <h2>Packages</h2>
 * <h3>Naive</h3>
 * A simple, naive implementation with varying implementations. <br/>
 * This approach suffers from class explosion, it needs a separate class
 * for each combination of shape and underlying implementation.
 *
 * <h3>DP</h3>
 * An approach that separates abstraction with underlying implementation variations <br/>
 * This approach de-couples the shapes (abstraction) and the drawing programs (underlying implementation).
 *
 * <h3>Provided</h3>
 * This package represents functionality that is not developed and cannot be modified.
 *
 * <h2>Architecture Design Examples</h2>
 * Please see src/main/resources/bridge dir
 *
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
package io.gitlab.cylearningit.designpatterns.bridge;
