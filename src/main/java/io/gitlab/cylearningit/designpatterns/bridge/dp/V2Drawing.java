package io.gitlab.cylearningit.designpatterns.bridge.dp;

import io.gitlab.cylearningit.designpatterns.bridge.provided.DP2;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class V2Drawing implements Drawing {
    @Override
    public void drawLine(double x1, double y1, double x2, double y2) {
        DP2.drawline(x1, x2, y1, y2);
    }

    @Override
    public void drawCircle(double x, double y, double r) {
        DP2.drawcircle(x, y, r);
    }
}
