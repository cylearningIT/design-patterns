package io.gitlab.cylearningit.designpatterns.bridge.naive;

import io.gitlab.cylearningit.designpatterns.bridge.provided.DP2;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class V2Rectangle extends Rectangle {
    public V2Rectangle(double x1, double y1, double x2, double y2) {
        super(x1, y1, x2, y2);
    }

    @Override
    protected void drawLine(double x1, double y1, double x2, double y2) {
        DP2.drawline(x1, x2, y1, y2);
    }
}
