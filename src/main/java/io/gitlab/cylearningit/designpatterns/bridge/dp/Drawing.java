package io.gitlab.cylearningit.designpatterns.bridge.dp;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public interface Drawing {
    void drawLine(double x1, double y1, double x2, double y2);
    void drawCircle(double x, double y, double r);
}
