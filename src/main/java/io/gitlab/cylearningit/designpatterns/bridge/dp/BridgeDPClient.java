package io.gitlab.cylearningit.designpatterns.bridge.dp;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class BridgeDPClient {
    public static void main(String[] args) {
        Drawing dp = new V1Drawing();
        Shape s1 = new Rectangle(dp, 1, 2, 3, 4);

        dp = new V2Drawing();
        Shape s2 = new Circle(dp, 1, 2, 3);

        s1.draw();
        s2.draw();
    }
}
