package io.gitlab.cylearningit.designpatterns.bridge.dp;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class Circle extends Shape {
    protected double x;
    protected double y;
    protected double r;

    public Circle(Drawing dp, double x, double y, double r) {
        super(dp);
        this.x = x;
        this.y = y;
        this.r = r;
    }

    @Override
    public void draw() {
        drawCircle(x, y, r);
    }
}
