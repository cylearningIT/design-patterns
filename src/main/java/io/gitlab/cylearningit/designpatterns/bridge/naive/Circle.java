package io.gitlab.cylearningit.designpatterns.bridge.naive;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public abstract class Circle extends Shape {
    protected double _x;
    protected double _y;
    protected double _r;

    public Circle(double x, double y, double r) {
        this._x = x;
        this._y = y;
        this._r = r;
    }

    @Override
    public void draw() {
        drawCircle(_x, _y, _r);
    }

    protected abstract void drawCircle(double x, double y, double r);
}
