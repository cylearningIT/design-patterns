package io.gitlab.cylearningit.designpatterns.bridge.naive;

/**
 *
 * x1     x2
 * y2 --- y2
 * |      |
 * x1 --- x2
 * y1     y1
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public abstract class Rectangle extends Shape {
    protected double _x1;
    protected double _y1;
    protected double _x2;
    protected double _y2;

    public Rectangle(double x1, double y1, double x2, double y2) {
        this._x1 = x1;
        this._y1 = y1;
        this._x2 = x2;
        this._y2 = y2;
    }

    @Override
    public void draw() {
        drawLine(_x1, _y2, _x2, _y2);
        drawLine(_x2, _y2, _x2, _y1);
        drawLine(_x2, _y1, _x1, _y1);
        drawLine(_x1, _y1, _x1, _y2);
    }

    protected abstract void drawLine(double x1, double y1,
                                     double x2, double y2);
}
