package io.gitlab.cylearningit.designpatterns.bridge.naive;

import io.gitlab.cylearningit.designpatterns.bridge.provided.DP2;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class V2Circle extends Circle {
    public V2Circle(double x, double y, double r) {
        super(x, y, r);
    }

    @Override
    protected void drawCircle(double x, double y, double r) {
        DP2.drawcircle(x, y, r);
    }
}
