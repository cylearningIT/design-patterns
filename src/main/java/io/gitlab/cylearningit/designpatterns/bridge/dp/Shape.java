package io.gitlab.cylearningit.designpatterns.bridge.dp;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public abstract class Shape {
    private Drawing _dp;

    public Shape(Drawing dp) {
        this._dp = dp;
    }

    public abstract void draw();

    protected void drawLine(double x1, double y1, double x2, double y2) {
        _dp.drawLine(x1, y1, x2, y2);
    }

    protected void drawCircle(double x, double y, double r) {
        _dp.drawCircle(x, y, r);
    }
}
