package io.gitlab.cylearningit.designpatterns.bridge.provided;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
public class DP2 {
    public static void drawline(double x1, double x2,
                                double y1, double y2) {
        log.info("DP2 - Draw a line between points: ({},{}) and ({},{})", x1, y1, x2, y2);
    }

    public static void drawcircle(double x, double y, double r) {
        log.info("DP2 - Draw a circle centered on ({},{}) with radius {}", x, y, r);
    }
}
