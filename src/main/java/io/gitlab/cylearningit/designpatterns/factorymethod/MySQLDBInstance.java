package io.gitlab.cylearningit.designpatterns.factorymethod;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
public class MySQLDBInstance implements DBInstance {
    private final String name;

    public MySQLDBInstance(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<String> performQuery(String query) {
        log.info("Performing {}", query);

        if (query.contains("SELECT")) {
            return Arrays.asList("Entry #1", "Entry #2");
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public String toString() {
        return name;
    }
}
