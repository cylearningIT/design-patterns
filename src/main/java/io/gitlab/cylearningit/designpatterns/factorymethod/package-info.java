/**
 * Classes for exploring the Factory Method Design Pattern <br/> <br/>
 * <h2>Summary</h2>
 * The Factory Method pattern is useful whenever we need to instantiate and use an object
 * but we want to defer the rules of instantiating it to some derived class.
 *
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
package io.gitlab.cylearningit.designpatterns.factorymethod;
