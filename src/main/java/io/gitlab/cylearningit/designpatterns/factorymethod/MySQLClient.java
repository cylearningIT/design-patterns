package io.gitlab.cylearningit.designpatterns.factorymethod;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class MySQLClient extends SQLClient {

    public static final String MY_SQL_COMMENT = " -- MySQL";

    @Override
    protected DBInstance makeDB(String dbName) {
        return new MySQLDBInstance(dbName);
    }

    @Override
    protected String formatConnect(DBInstance db) {
        return "CONNECT " + db.getName() + MY_SQL_COMMENT;
    }

    @Override
    protected String formatSelect(String tableName) {
        return "SELECT * from " + tableName + MY_SQL_COMMENT;
    }
}
