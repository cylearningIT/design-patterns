package io.gitlab.cylearningit.designpatterns.factorymethod;

import java.util.List;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public interface DBInstance {
    String getName();
    List<String> performQuery(String query);
}
