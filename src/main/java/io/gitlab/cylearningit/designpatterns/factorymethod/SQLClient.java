package io.gitlab.cylearningit.designpatterns.factorymethod;

import java.util.List;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
public abstract class SQLClient {
    public List<String> selectAll(String dbName, String tableName) {
        final DBInstance db = makeDB(dbName);

        String connectQuery = formatConnect(db);
        performQuery(db, connectQuery);

        String selectQuery = formatSelect(tableName);
        return performQuery(db, selectQuery);
    }

    protected abstract DBInstance makeDB(String dbName);

    protected abstract String formatConnect(DBInstance db);

    protected abstract String formatSelect(String tableName);

    private List<String> performQuery(DBInstance db, String query) {
        return db.performQuery(query);
    }
}
