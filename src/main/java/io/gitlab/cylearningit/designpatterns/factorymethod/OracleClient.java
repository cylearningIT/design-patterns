package io.gitlab.cylearningit.designpatterns.factorymethod;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class OracleClient extends SQLClient {

    public static final String ORACLE_COMMENT = " -- Oracle";

    @Override
    protected DBInstance makeDB(String dbName) {
        return new OracleDBInstance(dbName);
    }

    @Override
    protected String formatConnect(DBInstance db) {
        return "connect " + db.getName() + ORACLE_COMMENT;
    }

    @Override
    protected String formatSelect(String tableName) {
        return "select * from " + tableName + ORACLE_COMMENT;
    }
}
