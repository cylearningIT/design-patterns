package io.gitlab.cylearningit.designpatterns.factorymethod;

import java.util.List;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
public class FactoryMethodClient {
    public static void main(String[] args) {
        final List<String> mySqlStrings = new MySQLClient().selectAll("DB1-M", "T1-M");
        log.info("Retrieved from MySQL: {}", mySqlStrings);

        final List<String> oracleStrings = new OracleClient().selectAll("DB2-O", "t2-O");
        log.info("Retrieved from Oracle: {}", oracleStrings);
    }
}
