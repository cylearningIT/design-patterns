/**
 * Classes for exploring the Observer Design Pattern <br/> <br/>
 * <h2>Summary</h2>
 * The Observer Design Pattern provides a way to define a one-to-many dependency between objects
 * so that when one object (the <i>Subject</i>) changes state, all its dependents (the <i>Observers</i>)
 * are notifies in order to take action.
 * <br />
 * The Subject knows its Observers, but it is the responsibility of the Observers to register
 * with it. This decouples the two entities and allows for more flexibility in adding a new
 * Observer if need be.
 * <br />
 * The Observer pattern is great when the list of objects (Observers) changes dynamically,
 * or is in any way not fixed. But it is unnecessary complexity in cases where the dependencies
 * are fixed (or virtually so).
 *
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
package io.gitlab.cylearningit.designpatterns.observer;
