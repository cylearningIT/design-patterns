package io.gitlab.cylearningit.designpatterns.observer;

/**
 * An interface to be implemented by all Observer classes
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public interface Observer<T> {
    void update(T observable);
}
