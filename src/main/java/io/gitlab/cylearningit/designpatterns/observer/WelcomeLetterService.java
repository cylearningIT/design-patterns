package io.gitlab.cylearningit.designpatterns.observer;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
public class WelcomeLetterService implements Observer<Customer>, AutoCloseable {
    public WelcomeLetterService() {
        Customer.registerObserver(this);
    }

    @Override
    public void update(Customer customer) {
        log.info("Welcome, {}", customer.getName());
    }

    @Override
    public void close() {
        Customer.unregisterObserver(this);
    }
}
