package io.gitlab.cylearningit.designpatterns.observer;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
public class Customer {
    private static final List<Observer<Customer>> observers = new ArrayList<>();

    private final String name;
    private final String id;
    private final String address;

    public Customer(String name, String id, String address) {
        super();
        this.name = name;
        this.id = id;
        this.address = address;

        log.info("Created new Customer named {} with id {}", name, id);
        log.info("Notifying {} observers", observers.size());
        notifyObservers(this);
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public static synchronized void registerObserver(Observer<Customer> observer) {
        observers.add(observer);
    }

    public static synchronized void unregisterObserver(Observer<Customer> observer) {
        observers.remove(observer);
    }

    private void notifyObservers(Customer observable) {
        observers.forEach(o -> o.update(observable));
    }
}
