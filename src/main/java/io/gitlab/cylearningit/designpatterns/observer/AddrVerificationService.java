package io.gitlab.cylearningit.designpatterns.observer;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
public class AddrVerificationService implements Observer<Customer>, AutoCloseable {
    public AddrVerificationService() {
        Customer.registerObserver(this);
    }



    @Override
    public void update(Customer customer) {
        log.info("Verifying id {} against address {}", customer.getId(), customer.getAddress());
    }

    @Override
    public void close() {
        Customer.unregisterObserver(this);
    }
}
