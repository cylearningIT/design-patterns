package io.gitlab.cylearningit.designpatterns.observer;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class ObserverClient {
    public static void main(String[] args) {
        // NOTE In a real world scenario, the services would be managed by some framework, e.g. Spring

        // Start Address Verification Service
        try (final AddrVerificationService addrVerificationService = new AddrVerificationService()) {
            // Start Welcome Letter Service
            try (final WelcomeLetterService welcomeLetterService = new WelcomeLetterService()) {
                new Customer("Kypros", "123", "8 Kristalli, Kato Polemidia, Limassol, Cyprus, 4152");
            }
            // Stop Welcome Letter Service

            new Customer("Elena", "456", "10 Iona Nikolaou, Agios Spiridonas, Limassol, Cyprus, 3050");
        }
        // Stop Address Verification Service

        new Customer("Nikoletta", "789", "48 Serifou, Melina Court 2 App 103, Zakaki, Limassol, Cyprus, 3046");
    }
}
