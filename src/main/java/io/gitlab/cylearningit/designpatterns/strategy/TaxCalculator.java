package io.gitlab.cylearningit.designpatterns.strategy;

import java.math.BigDecimal;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public interface TaxCalculator {
    BigDecimal calculate(Saleable itemSold, BigDecimal quantity);
}
