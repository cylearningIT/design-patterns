package io.gitlab.cylearningit.designpatterns.strategy.strategies;

import java.math.BigDecimal;

import io.gitlab.cylearningit.designpatterns.strategy.Saleable;
import io.gitlab.cylearningit.designpatterns.strategy.TaxCalculator;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class CYTaxCalculator implements TaxCalculator {
    private static final BigDecimal TAX_RATE = BigDecimal.valueOf(0.19);

    @Override
    public BigDecimal calculate(Saleable itemSold, BigDecimal quantity) {
        return quantity.multiply(itemSold.getPrice()).multiply(TAX_RATE);
    }
}
