package io.gitlab.cylearningit.designpatterns.strategy;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Data
@AllArgsConstructor
public class Saleable {
    private String name;
    private BigDecimal price;
}
