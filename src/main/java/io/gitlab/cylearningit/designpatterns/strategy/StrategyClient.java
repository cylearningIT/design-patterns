package io.gitlab.cylearningit.designpatterns.strategy;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Locale;

import io.gitlab.cylearningit.designpatterns.strategy.strategies.CYTaxCalculator;
import io.gitlab.cylearningit.designpatterns.strategy.strategies.USTaxCalculator;
import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
public class StrategyClient {
    public static void main(String[] args) {
        TaxCalculator taxCalculator = Configuration.getTaxCalculator(Locale.forLanguageTag("el-CY"));
        SalesOrder order = new SalesOrder(taxCalculator);
        Saleable sugar = new Saleable("Sugar granulated 500gr", BigDecimal.valueOf(2.56));
        Saleable cucumber = new Saleable("Cucumber", BigDecimal.valueOf(0.75));
        order.addItem(sugar);
        order.addItem(cucumber, BigDecimal.valueOf(1.5));

        final BigDecimal totalCost = order.calculateTotalCost();
        log.info("Your order costs {} euro", totalCost.setScale(2, RoundingMode.HALF_DOWN).toPlainString());
    }

    private static class Configuration {
        public static TaxCalculator getTaxCalculator(Locale locale) {
            TaxCalculator taxCalculator = new USTaxCalculator();
            if (!Locale.US.getCountry().equals(locale.getCountry())) {
                taxCalculator = new CYTaxCalculator();
            }
            return taxCalculator;
        }
    }
}
