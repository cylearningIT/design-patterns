/**
 * Classes for exploring the Strategy Design Pattern <br/> <br/>
 * <h2>Summary</h2>
 * The Strategy Design Pattern is a way to define a family of algorithms.
 * In practice, Strategy can be used to encapsulate any kind of business rule.
 *
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
package io.gitlab.cylearningit.designpatterns.strategy;
