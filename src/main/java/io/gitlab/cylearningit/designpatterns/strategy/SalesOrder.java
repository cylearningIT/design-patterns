package io.gitlab.cylearningit.designpatterns.strategy;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class SalesOrder {
    private final TaxCalculator taxCalculator;
    private final Map<Saleable, BigDecimal> itemsSold = new HashMap<>();

    public SalesOrder(TaxCalculator taxCalculator) {
        this.taxCalculator = taxCalculator;
    }

    public void addItem(Saleable item) {
        addItem(item, BigDecimal.ONE);
    }

    public void addItem(Saleable item, BigDecimal quantity) {
        itemsSold.merge(item, quantity, BigDecimal::add);
    }

    public BigDecimal calculateTotalCost() {
        BigDecimal totalCost = BigDecimal.ZERO;

        for (Map.Entry<Saleable, BigDecimal> entry : itemsSold.entrySet()) {
            final Saleable item = entry.getKey();
            final BigDecimal quantity = entry.getValue();
            final BigDecimal itemTax = taxCalculator.calculate(item, quantity);
            totalCost = totalCost.add(item.getPrice().multiply(quantity)).add(itemTax);
        }

        return totalCost;
    }
}
