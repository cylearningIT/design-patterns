/**
 * Classes for exploring the Template Method Design Pattern <br/> <br/>
 * <h2>Summary</h2>
 * The Template Method pattern is useful whenever there is a common procedure
 * that needs to be followed at a high level, but the implementation of some
 * of the steps can vary.
 * <br />
 * This is achieved by extracting the common procedure in an abstract class,
 * where the varying steps are defined using virtual methods.
 * The subclasses are then responsible for implementing these steps.
 * <br />
 * This approach is good for code reuse and is also helpful in ensuring the
 * required steps are implemented.
 * <br />
 * <br />
 * <h2>Note:</h2>
 * In case a step can vary independently, the Strategy pattern can be utilised
 * to enable that.
 *
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
package io.gitlab.cylearningit.designpatterns.templatemethod;
