package io.gitlab.cylearningit.designpatterns.templatemethod;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class TemplateMethodClient {
    public static void main(String[] args) {
        new MySQLClient().selectAll("DB1-M", "T1-M");

        new OracleClient().selectAll("DB2-O", "t2-O");
    }
}
