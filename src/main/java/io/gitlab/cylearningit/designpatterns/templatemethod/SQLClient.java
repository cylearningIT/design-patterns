package io.gitlab.cylearningit.designpatterns.templatemethod;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
public abstract class SQLClient {
    public List<String> selectAll(String dbName, String tableName) {
        String connectQuery = formatConnect(dbName);
        performQuery(dbName, connectQuery);

        String selectQuery = formatSelect(tableName);
        return performQuery(dbName, selectQuery);
    }

    protected abstract String formatConnect(String dbName);

    protected abstract String formatSelect(String tableName);

    private List<String> performQuery(String dbName, String query) {
        log.info("Performing query: {} on {}", query, dbName);

        if (query.contains("SELECT") || query.contains("select")) {
            return Arrays.asList("Entry #1", "Entry #2");
        } else {
            return Collections.emptyList();
        }
    }
}
