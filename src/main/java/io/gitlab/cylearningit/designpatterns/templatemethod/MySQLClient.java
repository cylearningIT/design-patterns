package io.gitlab.cylearningit.designpatterns.templatemethod;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class MySQLClient extends SQLClient {

    public static final String MY_SQL_COMMENT = " -- MySQL";

    @Override
    protected String formatConnect(String dbName) {
        return "CONNECT " + dbName + MY_SQL_COMMENT;
    }

    @Override
    protected String formatSelect(String tableName) {
        return "SELECT * from " + tableName + MY_SQL_COMMENT;
    }
}
