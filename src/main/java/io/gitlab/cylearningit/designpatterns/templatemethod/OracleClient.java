package io.gitlab.cylearningit.designpatterns.templatemethod;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class OracleClient extends SQLClient {

    public static final String ORACLE_COMMENT = " -- Oracle";

    @Override
    protected String formatConnect(String dbName) {
        return "connect " + dbName + ORACLE_COMMENT;
    }

    @Override
    protected String formatSelect(String tableName) {
        return "select * from " + tableName + ORACLE_COMMENT;
    }
}
