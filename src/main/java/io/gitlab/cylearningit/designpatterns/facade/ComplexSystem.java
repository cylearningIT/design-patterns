package io.gitlab.cylearningit.designpatterns.facade;

/**
 * A "complex" system providing all basic mathematical operations for various combinations of input type
 *
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class ComplexSystem {
    // region Addition
    public static int add(int num1, int num2) {
        return num1 + num2;
    }

    public static float add(float num1, float num2) {
        return num1 + num2;
    }

    public static float add(int num1, float num2) {
        return num1 + num2;
    }

    public static double add(double num1, double num2) {
        return num1 + num2;
    }

    public static double add(int num1, double num2) {
        return num1 + num2;
    }

    public static double add(float num1, double num2) {
        return num1 + num2;
    }
    // endregion

    // region Subtraction
    public static int subtract(int num1, int num2) {
        return num1 - num2;
    }

    public static float subtract(float num1, float num2) {
        return num1 - num2;
    }

    public static float subtract(int num1, float num2) {
        return num1 - num2;
    }

    public static double subtract(double num1, double num2) {
        return num1 - num2;
    }

    public static double subtract(int num1, double num2) {
        return num1 - num2;
    }

    public static double subtract(float num1, double num2) {
        return num1 - num2;
    }
    // endregion

    // region Multiplication
    public static int multiply(int num1, int num2) {
        return num1 * num2;
    }

    public static float multiply(float num1, float num2) {
        return num1 * num2;
    }

    public static float multiply(int num1, float num2) {
        return num1 * num2;
    }

    public static double multiply(double num1, double num2) {
        return num1 * num2;
    }

    public static double multiply(int num1, double num2) {
        return num1 * num2;
    }

    public static double multiply(float num1, double num2) {
        return num1 * num2;
    }
    // endregion

    // region Division
    /**
     * Unsafe division operation. Divisor is supposed to be non-zero
     */
    public static int divide(int divident, int divisor) {
        return divident / divisor;
    }

    /**
     * Unsafe division operation. Divisor is supposed to be non-zero
     */
    public static double divide(float divident, float divisor) {
        return divident / divisor;
    }

    /**
     * Unsafe division operation. Divisor is supposed to be non-zero
     */
    public static double divide(int divident, float divisor) {
        return divident / divisor;
    }

    /**
     * Unsafe division operation. Divisor is supposed to be non-zero
     */
    public static double divide(int divident, double divisor) {
        return divident / divisor;
    }

    /**
     * Unsafe division operation. Divisor is supposed to be non-zero
     */
    public static double divide(float divident, double divisor) {
        return divident / divisor;
    }

    /**
     * Unsafe division operation. Divisor is supposed to be non-zero
     */
    public static double divide(double divident, double divisor) {
        return divident / divisor;
    }
    // endregion
}
