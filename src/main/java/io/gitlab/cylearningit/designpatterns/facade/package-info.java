/**
 * Classes for exploring the Facade Design Pattern <br/> <br/>
 * <h2>Summary</h2>
 * The Facade pattern is so named because it puts up a new front (a facade) in front of the original system.
 * <ul>
 *   <li>You do not need to use all of the functionality of a complex system and can create a new class that contains all of the rules for accessing that system. If this is a subset of the original system, as it usually is, the API that you create in new class should be much simpler than the original system's API.</li>
 *   <li>You want to encapsulate or hide the original system.</li>
 *   <li>You want to use the functionality of the original system and want to add some new functionality as well.</li>
 *   <li>The cost of writing this new class is less than the cost of every body learning how to use the original system or is less than you would spend on maintenance in the future.</li>
 * </ul>
 *
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
package io.gitlab.cylearningit.designpatterns.facade;
