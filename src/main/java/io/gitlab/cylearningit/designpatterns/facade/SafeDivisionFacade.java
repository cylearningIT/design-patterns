package io.gitlab.cylearningit.designpatterns.facade;

import java.util.Optional;

/**
 * A facade extending the division subset of functionality of the {@link ComplexSystem} with a safety check for division by zero
 *
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class SafeDivisionFacade {
    public static int divide(int divident, int divisor) {
        return evaluateInfinity(divident, divisor)
            .orElseGet(() -> ComplexSystem.divide(divident, divisor));
    }

    public static double divide(float divident, float divisor) {
        return evaluateInfinity(divident, divisor)
            .orElseGet(() -> (float) ComplexSystem.divide(divident, divisor));
    }

    public static double divide(double divident, double divisor) {
        return evaluateInfinity(divident, divisor)
            .orElseGet(() -> ComplexSystem.divide(divident, divisor));
    }

    private static Optional<Integer> evaluateInfinity(int divident, int divisor) {
        Integer infinity = null;
        if (divisor == 0) {
            infinity = divident < 0 ? Integer.MIN_VALUE : Integer.MAX_VALUE;
        }

        return Optional.ofNullable(infinity);
    }

    private static Optional<Float> evaluateInfinity(float divident, float divisor) {
        Float infinity = null;
        if (divisor == 0) {
            infinity = divident < 0 ? Float.MIN_VALUE : Float.MAX_VALUE;
        }

        return Optional.ofNullable(infinity);
    }

    private static Optional<Double> evaluateInfinity(double divident, double divisor) {
        Double infinity = null;
        if (divisor == 0) {
            infinity = divident < 0 ? Double.MIN_VALUE : Double.MAX_VALUE;
        }

        return Optional.ofNullable(infinity);
    }
}
