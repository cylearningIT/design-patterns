package io.gitlab.cylearningit.designpatterns.facade;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
public class FacadeClient {
    public static void main(String[] args) {
        log.info("2 x 3 = {}", IntegerOperationsFacade.multiply(2, 3));

        log.info("-1 / 0 = {}", SafeDivisionFacade.divide(-1, 0));
    }
}
