package io.gitlab.cylearningit.designpatterns.facade;

/**
 * A simplified Facade focused on the subset of functionality of {@link ComplexSystem} related to Integer operations
 *
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class IntegerOperationsFacade {
    public static int add(int num1, int num2) {
        return ComplexSystem.add(num1, num2);
    }

    public static int subtract(int num1, int num2) {
        return ComplexSystem.subtract(num1, num2);
    }

    public static int multiply(int num1, int num2) {
        return ComplexSystem.multiply(num1, num2);
    }

    public static int divide(int num1, int num2) {
        return ComplexSystem.divide(num1, num2);
    }
}
