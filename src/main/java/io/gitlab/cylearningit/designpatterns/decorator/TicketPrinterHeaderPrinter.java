package io.gitlab.cylearningit.designpatterns.decorator;

import org.springframework.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
public class TicketPrinterHeaderPrinter extends TicketPrinterDecorator {
    private final String additionalText;

    protected TicketPrinterHeaderPrinter(TicketPrinter ticketPrinter) {
        this(ticketPrinter, "");
    }

    protected TicketPrinterHeaderPrinter(TicketPrinter ticketPrinter, String additionalText) {
        super(ticketPrinter);
        this.additionalText = additionalText;
    }

    @Override
    public void printTicket() {
        printTicketHeader();
        super.printTicket();
    }

    private void printTicketHeader() {
        StringBuilder sb = new StringBuilder("Ticket Header");
        if (!StringUtils.isEmpty(additionalText)) {
            sb.append(" - ").append(additionalText);
        }

        log.info(sb.toString());
    }
}
