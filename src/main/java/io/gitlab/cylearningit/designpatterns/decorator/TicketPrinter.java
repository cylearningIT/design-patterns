package io.gitlab.cylearningit.designpatterns.decorator;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public interface TicketPrinter {
    void printTicket();
}
