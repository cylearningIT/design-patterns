package io.gitlab.cylearningit.designpatterns.decorator;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public abstract class TicketPrinterDecorator implements TicketPrinter {
    final TicketPrinter ticketPrinter;

    protected TicketPrinterDecorator(TicketPrinter ticketPrinter) {
        this.ticketPrinter = ticketPrinter;
    }

    @Override
    public void printTicket() {
        if (null != ticketPrinter) {
            ticketPrinter.printTicket();
        }
    }
}
