/**
 * Classes for exploring the Decorator Design Pattern <br/> <br/>
 * <h2>Summary</h2>
 * The Decorator pattern is a way to add additional function(s)
 * before/after an existing function, dynamically.
 *
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
package io.gitlab.cylearningit.designpatterns.decorator;
