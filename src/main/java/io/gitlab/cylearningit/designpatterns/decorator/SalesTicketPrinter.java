package io.gitlab.cylearningit.designpatterns.decorator;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
public class SalesTicketPrinter implements TicketPrinter {
    @Override
    public void printTicket() {
        log.info("Sales Ticket");
    }
}
