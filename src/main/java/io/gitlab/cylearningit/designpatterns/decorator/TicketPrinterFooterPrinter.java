package io.gitlab.cylearningit.designpatterns.decorator;

import org.springframework.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
public class TicketPrinterFooterPrinter extends TicketPrinterDecorator {
    private final String additionalText;

    protected TicketPrinterFooterPrinter(TicketPrinter ticketPrinter) {
        this(ticketPrinter, "");
    }

    protected TicketPrinterFooterPrinter(TicketPrinter ticketPrinter, String additionalText) {
        super(ticketPrinter);
        this.additionalText = additionalText;
    }

    @Override
    public void printTicket() {
        super.printTicket();
        printTicketFooter();
    }

    private void printTicketFooter() {
        StringBuilder sb = new StringBuilder("Ticket Footer");
        if (!StringUtils.isEmpty(additionalText)) {
            sb.append(" - ").append(additionalText);
        }

        log.info(sb.toString());
    }
}
