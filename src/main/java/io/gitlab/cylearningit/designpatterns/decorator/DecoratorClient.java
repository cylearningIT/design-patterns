package io.gitlab.cylearningit.designpatterns.decorator;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
public class DecoratorClient {
    public static void main(String[] args) {
        // Simple ticket with a header and a footer
        final TicketPrinterHeaderPrinter hdrFtrTicket = new TicketPrinterHeaderPrinter(new TicketPrinterFooterPrinter(new SalesTicketPrinter()));
        log.info("Printing ticket with a header and a footer");
        hdrFtrTicket.printTicket();

        log.info("   ---   ---   ---   ---");

        // Ticket with two headers and a footer
        final TicketPrinterHeaderPrinter twoHdrFtrTicket = new TicketPrinterHeaderPrinter(new TicketPrinterFooterPrinter(new TicketPrinterHeaderPrinter(new SalesTicketPrinter(), "2")), "1");
        log.info("Printing ticket with two headers and a footer");
        twoHdrFtrTicket.printTicket();
    }
}
