package io.gitlab.cylearningit.designpatterns.adapter;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class AdapterClient {
    public static void main(String[] args) {
        List<Shape> shapes = new ArrayList<>();

        shapes.add(createPoint(1, 2));
        shapes.add(createPoint(-1, -2));
        shapes.add(createLine(-10f, 0.3f, 0f, 2f));
        shapes.add(createLine(20f, -5.3f, 90f, 12f));
        shapes.add(createSquare(-15, 5, 45, 3));
        shapes.add(createCircle(25, -4, 12));
        shapes.add(createCircle(-25, -8, 20));

        shapes.forEach(Shape::fill);
        shapes.forEach(Shape::display);

        // Hide all shapes in the negative X's
        shapes.stream().filter(shape -> shape.getLocation().getX() < 0).forEach(Shape::undisplay);
    }

    private static Point createPoint(float x, float y) {
        Point point = new Point();
        point.setLocation(getPoint2D(x, y));
        return point;
    }

    private static Line createLine(float x, float y, float rotation, float length) {
        Line line = new Line();
        line.setLocation(getPoint2D(x, y));
        line.setRotation(rotation);
        line.setLength(length);
        return line;
    }

    private static Square createSquare(float x, float y, float rotation, float sideLength) {
        Square square = new Square();
        square.setLocation(getPoint2D(x, y));
        square.setRotation(rotation);
        square.setSideLength(sideLength);
        return square;
    }

    private static Circle createCircle(float x, float y, float radius) {
        Circle circle = new Circle();
        circle.setLocation(getPoint2D(x, y));
        circle.setRadius(radius);
        return circle;
    }

    private static Point2D.Float getPoint2D(float x, float y) {
        return new Point2D.Float(x, y);
    }
}
