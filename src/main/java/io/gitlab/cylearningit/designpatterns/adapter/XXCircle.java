package io.gitlab.cylearningit.designpatterns.adapter;

import java.awt.*;
import java.awt.geom.Point2D;

import lombok.extern.slf4j.Slf4j;

/**
 * A class that implementa all the needed functionality of {@link Shape} but does not comply with its API
 *
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
public class XXCircle {
    private Point2D location;
    private Color color;
    private float radius;

    public Point2D getLocation() {
        return location;
    }

    public void setLocation(Point2D location) {
        this.location = location;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public void displayIt() {
        log.info("An xxCircle is now visible at {} with radius {}", getLocation(), getRadius());
    }

    public void undisplayIt() {
        log.info("An xxCircle is no longer visible at {} with radius {}", getLocation(), getRadius());
    }

    public void fillIt() {
        log.info("Filling an xxCircle at {} with radius {}", getLocation(), getRadius());
    }
}
