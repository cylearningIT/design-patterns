/**
 * Classes for exploring the Adapter Design Pattern <br/> <br/>
 * <h2>Summary</h2>
 * The Adapter pattern is a very useful pattern that converts the interface of a class (or classes) into another
 * interface, which we need the class to have. It is implemented by creating a new class with the desired interface and
 * then wrapping the original class methods to effectively contain the adapted object. <br/> <br/>
 *
 * <h2>Difference with Facade pattern</h2>
 * A Facade <i>simplifies</i> an interface while an Adapter <i>converts</i> the interface into a preexisting interface.
 *
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
package io.gitlab.cylearningit.designpatterns.adapter;
