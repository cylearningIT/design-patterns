package io.gitlab.cylearningit.designpatterns.adapter;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
public class Line extends Shape {
    private double rotation;
    private double length;

    public double getRotation() {
        return rotation;
    }

    public void setRotation(double rotation) {
        this.rotation = rotation;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public void display() {
        log.info("A line is now visible at {} with orientation {} and length {}", getLocation(), getRotation(), getLength());
    }

    @Override
    public void undisplay() {
        log.info("A line is no longer visible at {} with rotation {} and length {}", getLocation(), getRotation(), getLength());
    }

    @Override
    public void fill() {
        log.info("Filling a line at {} with rotation {} and length {}", getLocation(), getRotation(), getLength());
    }
}
