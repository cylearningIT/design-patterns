package io.gitlab.cylearningit.designpatterns.adapter;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
public class Point extends Shape {
    @Override
    public void display() {
        log.info("A point is now visible at {}", getLocation());
    }

    @Override
    public void undisplay() {
        log.info("A point is no longer visible at {}", getLocation());
    }

    @Override
    public void fill() {
        log.info("Filling a single point at {}", getLocation());
    }
}
