package io.gitlab.cylearningit.designpatterns.adapter;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
public class Square extends Shape {
    private double rotation;
    private double sideLength;

    public double getRotation() {
        return rotation;
    }

    public void setRotation(double rotation) {
        this.rotation = rotation;
    }

    public double getSideLength() {
        return sideLength;
    }

    public void setSideLength(double sideLength) {
        this.sideLength = sideLength;
    }

    @Override
    public void display() {
        log.info("A square is now visible at {} with rotation {} and length {}", getLocation(), getRotation(), getSideLength());
    }

    @Override
    public void undisplay() {
        log.info("A square is no longer visible at {} with rotation {} and length {}", getLocation(), getRotation(), getSideLength());
    }

    @Override
    public void fill() {
        log.info("Filling a square at {} with rotation {} and length {}", getLocation(), getRotation(), getSideLength());
    }
}
