package io.gitlab.cylearningit.designpatterns.adapter;

import java.awt.*;
import java.awt.geom.Point2D;

/**
 * A base abstract class through which the client will use the objects in a uniform manner
 *
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public abstract class Shape {
    private Point2D location;
    private Color color;

    public abstract void display();
    public abstract void undisplay();
    public abstract void fill();

    public void setLocation(Point2D location) {
        this.location = location;
    }

    public Point2D getLocation() {
        return this.location;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
