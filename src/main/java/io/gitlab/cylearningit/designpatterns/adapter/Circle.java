package io.gitlab.cylearningit.designpatterns.adapter;

import java.awt.*;
import java.awt.geom.Point2D;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class Circle extends Shape {
    private XXCircle xxCircle;

    public Circle() {
        this.xxCircle = new XXCircle();
    }

    @Override
    public void setLocation(Point2D location) {
        xxCircle.setLocation(location);
    }

    @Override
    public Point2D getLocation() {
        return xxCircle.getLocation();
    }

    @Override
    public Color getColor() {
        return xxCircle.getColor();
    }

    @Override
    public void setColor(Color color) {
        xxCircle.setColor(color);
    }

    @Override
    public void display() {
        xxCircle.displayIt();
    }

    @Override
    public void undisplay() {
        xxCircle.undisplayIt();
    }

    @Override
    public void fill() {
        xxCircle.fillIt();
    }

    public void setRadius(float radius) {
        xxCircle.setRadius(radius);
    }

    public float getRadius() {
        return xxCircle.getRadius();
    }
}
