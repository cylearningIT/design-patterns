package io.gitlab.cylearningit.designpatterns.abstractfactory;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
public class HRDD {
    void visualize() {
        log.info("HRDD - Visualize high res, high demand");
    }
}
