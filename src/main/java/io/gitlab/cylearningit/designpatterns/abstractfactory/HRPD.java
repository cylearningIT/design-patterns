package io.gitlab.cylearningit.designpatterns.abstractfactory;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
public class HRPD implements PrintDriver {
    @Override
    public void doPrint() {
        log.info("HRPD - Print high res, high demand");
    }
}
