package io.gitlab.cylearningit.designpatterns.abstractfactory;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public interface PrintDriver {
    void doPrint();
}
