package io.gitlab.cylearningit.designpatterns.abstractfactory;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
public class LRPD implements PrintDriver {
    @Override
    public void doPrint() {
        log.info("LRPD - Print low res, low demand");
    }
}
