package io.gitlab.cylearningit.designpatterns.abstractfactory;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public interface ResFactory {

    String LOW_RES = "LR";
    String CONFIG_KEY = "abstractfactory_key";
    String HIGH_RES = "HR";

    DisplayDriver getDisplayDriver();
    PrintDriver getPrintDriver();

    static ResFactory getInstance() {
        ResFactory factory;

        final String res = System.getProperty(CONFIG_KEY, LOW_RES);
        switch (res) {
            case HIGH_RES:
                factory = new HighResFactory();
                break;
            case LOW_RES:
            default:
                factory = new LowResFactory();
                break;
        }

        return factory;
    }
}
