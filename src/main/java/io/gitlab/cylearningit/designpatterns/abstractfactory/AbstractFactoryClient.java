package io.gitlab.cylearningit.designpatterns.abstractfactory;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class AbstractFactoryClient {
    public static void main(String[] args) {
        final ResFactory factory = ResFactory.getInstance();
        final DisplayDriver displayDriver = factory.getDisplayDriver();
        final PrintDriver printDriver = factory.getPrintDriver();

        displayDriver.doDraw();
        printDriver.doPrint();
    }
}
