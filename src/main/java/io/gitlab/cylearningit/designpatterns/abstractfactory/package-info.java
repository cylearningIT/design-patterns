/**
 * Classes for exploring the Abstract Factory Design Pattern <br/> <br/>
 * <h2>Summary</h2>
 * The Abstract Factory is used when you must coordinate the creation of <b>families of objects</b>.
 * It separates the logic that is choosing the family of objects to be instantiated,
 * from the logic that is using the created objects.
 *
 * <h2>Architecture Design Examples</h2>
 * Please see src/main/resources/abstractfactory dir
 * <ul>
 * <li><b>Abstract_Factory_Generic.png</b> shows a generic design for the Abstract Factory pattern </li>
 * <li><b>Abstract_Factory_Adapter.png</b> shows a compound design that combines the Abstract Factory with
 *                              the Adapter Design Pattern which is suitable for cases where the
 *                              different product versions cannot be explicitly abstracted as is.</li>
 * </ul>
 *
 * <h3>Notes</h3>
 * In this example I used the generic approach for the Print Drivers and the compound design for the Display Driver.
 *
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
package io.gitlab.cylearningit.designpatterns.abstractfactory;
