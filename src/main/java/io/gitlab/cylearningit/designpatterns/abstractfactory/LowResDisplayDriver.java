package io.gitlab.cylearningit.designpatterns.abstractfactory;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class LowResDisplayDriver implements DisplayDriver {
    private LRDD lrdd;

    public LowResDisplayDriver() {
        lrdd = new LRDD();
    }

    @Override
    public void doDraw() {
        lrdd.show();
    }
}
