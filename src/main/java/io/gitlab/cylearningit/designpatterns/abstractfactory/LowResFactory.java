package io.gitlab.cylearningit.designpatterns.abstractfactory;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class LowResFactory implements ResFactory {
    private DisplayDriver displayDriver;
    private PrintDriver printDriver;

    @Override
    public DisplayDriver getDisplayDriver() {
        if (null == displayDriver) {
            displayDriver = new LowResDisplayDriver();
        }

        return displayDriver;
    }

    @Override
    public PrintDriver getPrintDriver() {
        if (null == printDriver) {
            printDriver = new LRPD();
        }

        return printDriver;
    }
}
