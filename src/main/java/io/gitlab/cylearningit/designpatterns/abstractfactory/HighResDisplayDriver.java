package io.gitlab.cylearningit.designpatterns.abstractfactory;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class HighResDisplayDriver implements DisplayDriver {
    private HRDD hrdd;

    public HighResDisplayDriver() {
        hrdd = new HRDD();
    }

    @Override
    public void doDraw() {
        hrdd.visualize();
    }
}
